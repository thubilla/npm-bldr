import faker from 'faker';

import Application from './lib/core';

import sayHello from './lib/modules/hello';
import devMode from './lib/util/livereload-scripttag';

import Navbar from './lib/views/components/navbar.js';
import Modal from './lib/views/components/modal.js';
import HomePage from './lib/views/pages/home-page.js';

export default window.onload = function(){
	const app = new Application();
	const $header = document.querySelector('#header');
	const $container = document.querySelector('#container');

	document.body.setAttribute('id', 'app');

	if(location.href.includes('localhost') || location.href.includes('192')){
		devMode();

		window.app = app;
	}

	/*
		Define/Set Data Model Used to Populate Menu
	*/
	const links = app.data.addCollection('links');

	links.insert({ href: 'https://earthlyillusionaudio.bandcamp.com', text: 'Bandcamp' });
	links.insert({ href: 'https://soundcloud.com/earthlyillusion', text: 'Soundcloud' });
	links.insert({ href: 'https://unheardnotes.tumblr.com', text: 'Tumblr' });
	links.insert({ href: 'https://github.com/earthtone', text: 'Github' });
	links.insert({ href: 'https://codepen.io/earthtone/', text: 'Codepen' });

	/*
		Define Components/Pages/Views
	*/
	const $navbar = new Navbar({
		id: 'navbar',
		parentNode: $header,
		data: {
			links: app.data.getCollection('links').find()
		}
	});

	const $homePage = new HomePage({
		id: 'homePage',
		parentNode: $container,
		classList: [ 'container-inner' ],
		data: {
			text: sayHello()
		}
	});

	const $modal = new Modal({
		id: 'modal',
		parentNode: $container,
		classList: [ 'modal' ],
		data: {
			email: 'tonio.hubilla@gmail.com',
			name: 'Tonio Hubilla',
			things: [
				'websites',
				'electronic music'
			]
		}
	});
	
	/*
		Render Components/Pages/Views
	*/
	$navbar.render();
	$homePage.render();
	$modal.render();

	/*
		Attach Event Listeners
		Define Callbacks
	*/
	document.querySelector('.modal-trig').addEventListener('click', e => {
		app.publish('showModal', app, document.querySelector('#modal'))
	});
	
	app.subscribe('showModal', $homePage.showModal);

	window.addEventListener('click', e => {
		app.publish('hideModal', app, e, document.querySelector('#modal'))
	});
	
	app.subscribe('hideModal', $homePage.hideModal);

	document.querySelector('#menu-toggle').addEventListener('click', e => {
		app.publish('showMenu', app, document.querySelector('.menu'))
	});
	
	app.subscribe('showMenu', $navbar.showMenu);

	window.addEventListener('click', e => {
		$homePage.update({
			data: { text: sayHello(faker.lorem.word()) }
		});
		
		app.publish('changeButton', $homePage);
	});

	app.subscribe('changeButton', $homePage.render);
};

