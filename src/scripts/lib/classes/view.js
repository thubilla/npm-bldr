import yo from 'yo-yo';
import update from '../util/update-this';

export default class View {
	constructor(config){
		this.update = update;
		this.update(config);
		return this;
	}

	render(callback, ...args){
		const $el = this.html();

		if(this.parentNode.firstChild && this.parentNode.firstChild.id == this.id){
			yo.update(this.parentNode.firstChild, $el);
		} else {
			this.parentNode.appendChild($el);	
		}

		if(callback){
			callback(...args);
		}
	}
}