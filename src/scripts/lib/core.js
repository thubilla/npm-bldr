import loki from 'lokijs';
import Navigo from 'navigo';
import update from './util/update-this';

const defaultConfig = {
	api: '/api/v1',
	uri: location,
	dom: window.document,
	query: 'http://google.com/',
	fetch: window.fetch
};

export default class Application {
	constructor(config = defaultConfig){
		/**
			@method {update} => Bind Data to App
		*/
		this.update = update;
		/**
			@method {router} => Handle URL Routing
		*/
		this.router = new Navigo(null, true);
		this.data = new loki('db.json');
		/**
			Initialize App with Config Input
		*/
		this.update(config);
		return this;
	}

	subscribe(event, subscriber){
		if(!this.subscriptions){
			this.subscriptions = {};
		}

		if(!this.subscriptions[event]){
			this.subscriptions[event] = [];
		}

		if(typeof subscriber != 'function'){
			throw new TypeError('Subscriber is not a Function');
		}

		this.subscriptions[event].push(subscriber);
	}

	hasSubscription(event, subscriber){
		if(!this.subscriptions || !this.subscriptions[event]){
			return false;
		}

		return this.subscriptions[event].indexOf(subscriber) >= 0;
	}

	publish(event, ctx, ...args){
		if(!this.subscriptions || !this.subscriptions[event]){
			return;
		}

		return this.subscriptions[event].forEach(subscription => {
			try{
				subscription.apply(ctx, args);	
			}
			catch(e){
				console.error(e.message);
			}
		});
	}
}