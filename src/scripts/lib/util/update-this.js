export default function update(config){
	for(let k in config){
		if(config.hasOwnProperty(k)){
			this[k] = config[k];
		}
	}

	return this;
}