import yo from 'yo-yo';
import View from '../../classes/view.js';

export default class Modal extends View {
	constructor(config){
		super(config);
		return this;
	}

	html(){
		return yo`<div id="${this.id}" class="${this.classList.join(' ')}">
			<div id="info-modal" class="modal-content">
				<h2>My name is <a href="mailto:${this.data.email}">${this.data.name}</a>.</h2>
				<h5>I make ${this.data.things.join(' & ')}.</h5>
				<iframe width="50%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/277137328&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
			</div>
		</div>`;
	}
}