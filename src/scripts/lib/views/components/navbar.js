import yo from 'yo-yo';
import View from '../../classes/view';

export default class Navbar extends View {
	constructor(config){
		super(config);
		return this;
	}

	html(){
		return yo`<nav id="navbar">
			<button id="menu-toggle" type="button" class="btn">
				<span class="fa fa-navicon"></span>
			</button>
			<ul class="menu">
				${this.data.links.map(datum => {
					return yo`<li>
						<a href="${datum.href}">${datum.text}</a>
					</li>`;
				})}
			</ul>
		</nav>`;
	}

	showMenu(el){
		el.classList.toggle('open');
	}
}