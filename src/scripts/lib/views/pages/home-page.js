import yo from 'yo-yo';
import View from '../../classes/view.js';

export default class HomePage extends View {
	constructor(config){
		super(config);
		return this;
	}

	html(){
		return yo`<div id=${this.id} class="${this.classList.join(' ')}">
			<button id="welcome-button" class="modal-trig"><h6>${this.data.text}</h6></button>
		</div>`;
	}

	showModal(el){
		el.style.display = 'flex';
	}

	hideModal(e, el){
		if(e.target === el){
			el.style.display = 'none';
		}
	}
}