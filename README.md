Project Boilerplate
===================

## Using NPM as Task Runner/Build Tool
### Dependencies:
	- Node/NPM
	- PhantomJS

### Dev Dependencies:
	- Browserify
	- Babelify ('babel-preset-env')
	- Watchify
	- Node-Sass
	
	- Nodemon
	- Express
### Testing:
	- Eslint
	- Mocha
	- Karma

## Getting Started 
### Commands:
> npm install
>
> npm start
>	> npm build
> >		- browserify
> >		- node-sass
>	> npm watch
> >		- watchify
> >		- nodemon
>	> npm serve
> >		- nodemon (express)
> npm test
>	> karma start

NOTES ON ARCHITECTURE
=====================
[Taken from 'Creating A Scalable JavaScript Application Architecture' by Nicholas Zakas](https://www.youtube.com/watch?v=b5pFv9NB9fs&t=218s)

# Structure
|_*Environment (Browser)*_
	|_*Base Library (e.g. jQuery, ES2015/Modern Web APIs)*_
		|_*Application Core*_
			|_*Sandbox*_
				|_*Modules*_
				|_*Extensions*_

# Modules

## Rules
    - Hands to yourself
        + Only call your own methods or those on the sandbox
        + Don't access DOM elements outside of your box
        + Don't access non-native (e.g. window, document) global objects
    - Ask, don't take
        + Anything else you need, ask the sandbox
    - Don't leave "toys" around
        + Don't create global objects
    - Don't talk to strangers
        + Don't directly reference other modules


# Sandbox

- Defines the way modules interact with the rest of the world
- Modules only know the sandbox (don't know about each other)
- Also acts as "security guard" (mostly by convention)
- Take time to design correct interface

## Jobs
    -Consistency
    -Security
    -Communication

# Application Core

- Manage modules
    + Mediator Pattern
- AKA Application Controller
- Responsible for application life-cycle
    + Tells module when to initialize/shut down

## Jobs
    - Manage Module Life-Cycle
    - Enable inter-module communication
    - General error handling
    - Be extensible
        - more error handling
        - AJAX communication
        - new module capabilities
        - general utilities

### E.G.
> Ajax Extension
>   - Hide AJAX communication details
>   - Provide common request interface
>   - Provide common response interface
>   - Manage server failures
>   - Knows Entry/End Point
>   - Knows how to format request
>   - Knows how to parse response


# Base Library

- Project Scaffolding
- (Ideally) Only Application Core is aware of Base Library

## Jobs
    - Browser Normalization
    - General Purpose Utilities
        + Parsers/Serializers for Data (e.g. XML, JSON, etc.)
        + Object Manipulation
        + DOM Manipulation
        + AJAX Communication
    - Low-level Extensibility

# Dependencies

## Only *Base Library* is aware of environment (i.e. Browser)
## Only *Application Core* is aware of *Base Library*
## Only *Sandbox* is aware of *Application Core*
## *Modules* are only  aware of *Sandbox* and nothing else
## No part is aware of the whole application

### Advantages
    - Portable
        + Create New Projects from Scaffolding
    - Testable
        + Unit Testable
        + Everything built to interface => Interfaces easily stubbed
    - Modular
        + Layers are switchable/changeable
