const express = require('express');
const fs = require('fs');
const cors = require('cors');
const app = express();

function api(ln){
	var faker = require('faker'),
		data = { users: [] };

	for(let i = 0; i < ln; i++){
		data.users.push({
			id: i,
			name: faker.name.findName(),
			avatar: faker.internet.avatar(),
			bio: faker.lorem.paragraphs(2)
		});
	}

	return data;
}

app.use(cors());
app.set('port', process.env.PORT || 3000);
app.use(express.static('dist'));

app.get('/api/v1/', function(req, res){
	var data = api(5);
	res.send(data);
});

app.listen(app.get('port'), function(){
	const message = `Express started & is listening on port ${app.get('port')}; press Ctr-C to terminate`;
	console.log(message);
});
