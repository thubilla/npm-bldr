import assert from 'assert';
import Application from '../src/scripts/lib/core';

describe('Application Mediator', function(){
	var app, jocation;
	beforeEach(function(){
		jocation = {
			href:'http://some.thing.com?param=true#1234/page/0' 
		};

		app = new Application({
			api: 'http://hipsterjesus.com/api/',
			uri: jocation.href,
			data: ['red', 'yellow', 'blue']
		});
	});

	describe('.subscribe', function(){
		it('should store a function', function(){
			var subscriber = function(){};

			app.subscribe('event', subscriber);
			assert.equal(subscriber, app.subscriptions['event'][0]);
		});

		it('should store multiple functions', function(){
			var subscribers = [function(){}, function(){}];

			app.subscribe('event', subscribers[0]);
			app.subscribe('event', subscribers[1]);

			assert.deepEqual(subscribers, app.subscriptions['event']);
		});		
	});

	describe('.hasSubscription', function(){
		it('should return true when has subscription', function(){
			var subscriber = function(){};

			app.subscribe('event', subscriber);
			assert.ok(app.hasSubscription('event', subscriber));
		});

		it('should return false when no subscription', function(){
			assert.ok(!app.hasSubscription('event', function(){}));
		});
	});

	describe('.publish', function(){
		it('should call all subscribers', function(){
			var subscriber1 = function(){subscriber1.called = true;},
				subscriber2 = function(){subscriber2.called = true;};

			app.subscribe('event', subscriber1);
			app.subscribe('event', subscriber2);
			app.publish('event');

			assert.ok(subscriber1.called);
			assert.ok(subscriber2.called);
		});

		it('should pass through arguments', function(){
			var actual;

			app.subscribe('event', function(){
				actual = [...arguments];
			});

			app.publish('event', this, 'String', 1, 32);
			assert.deepEqual(['String', 1 ,32], actual);
		});

		it('should not fail if no subscriptions', function(){
			assert.doesNotThrow(function(){
				app.publish('event');
			});
		});

		it('should notify relevant subscribers only', function(){
			var calls = [];
			app.subscribe('event', function(){
				calls.push('event');
			});
			app.subscribe('other', function(){
				calls.push('other');
			});

			app.publish('other');

			assert.deepEqual(['other'], calls);
		});
	});

	describe('Error Handling', function(){
		it('should throw for uncallable subscriber', function(){
			assert.throws(function(){
				app.subscribe();
			}, 'TypeError');
		});

		it('should notify all even when some fail', function(){
			var subscriber1 = function(){throw new Error('Oops');},
				subscriber2 = function(){subscriber2.called = true;};

			app.subscribe('event', subscriber1);
			app.subscribe('event', subscriber2);
			app.publish('event');

			assert.ok(subscriber2.called);
		});
	});

	describe('Call Order', function(){
		it('should call subscribers in order added', function(){
			var calls = [],
				subscriber1 = function(){calls.push(subscriber1);},
				subscriber2 = function(){calls.push(subscriber2);};

			app.subscribe('event', subscriber1);
			app.subscribe('event', subscriber2);
			app.publish('event');

			assert.equal(subscriber1, calls[0]);
			assert.equal(subscriber2, calls[1]);
		});
	});
});
