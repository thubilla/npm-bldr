import assert from 'assert';
import sayHello from '../src/scripts/lib/modules/hello';

describe('Say Hello', function(){
	it('with a default value', function(){
		assert.equal(sayHello(), 'Hello World!');
	});

	it('with an input value', function(){
		assert.equal(sayHello('Jane'), 'Hello Jane!');
	});
});
