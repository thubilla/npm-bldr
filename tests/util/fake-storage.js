export default {
	data: {},
	getItem: function(key){
		return this.data[key];
	},
	setItem: function(key, value){
		data[key] = value;
	},
	removeItem: function(key){
		delete data[key];
	},
	hasItem: function(key){
		return this.data.hasOwnProperty(key);
	},
	keys: function(){
		return Object.keys(this.data);
	}
}