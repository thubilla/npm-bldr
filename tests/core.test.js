import assert from 'assert';
import Application from '../src/scripts/lib/core';

describe('Application Core', function(){
	var app, jocation;
	beforeEach(function(){
		jocation = {
			href:'http://some.thing.com?param=true#1234/page/0' 
		};

		app = new Application({
			api: 'http://hipsterjesus.com/api/',
			uri: jocation.href,
			data: ['red', 'yellow', 'blue']
		});
	});

	it('exists', function(){
		assert.ok(app);
	});

	it('is an object', function(){
		assert.equal(typeof app, 'object');
	});

	it('has accessible parameters', function(){
		app.param = true;
		assert.ok(app.param);

		app.param = false;
		assert.ok(!app.param);
	});

	it('inits with config parameters', function(){
		assert.equal(app.uri, jocation.href);
		assert.deepEqual(app.data, ['red', 'yellow', 'blue']);
	});
});